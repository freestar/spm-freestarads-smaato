// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 22.1.4
let package = Package(
    name: "FreestarAds-Smaato",
    defaultLocalization: "en",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-Smaato",
            targets: [
                "FreestarAds-Smaato",
                "FreestarAds-Smaato-Core",
                "OMSDK_Smaato",
                "SmaatoSDKBanner",
                "SmaatoSDKCore",
                "SmaatoSDKInterstitial",
                "SmaatoSDKNative",
                "SmaatoSDKOpenMeasurement",
                "SmaatoSDKRewardedAds",
                "SmaatoSDKRichMedia",
                "SmaatoSDKVideo"
             ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.30.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-Smaato",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/FreestarAds-Smaato.xcframework.zip",
            checksum: "a67e4225672fb1b6fab8ad77dcaaad7a781f4d3bb1b0ac23af620c1c7f55babb"
            ),
        .target(
            name: "FreestarAds-Smaato-Core",
                dependencies: [
                    .product(name: "FreestarAds", package: "spm-freestarads-core")
                ]
            ),
        .binaryTarget(
            name: "OMSDK_Smaato",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/OMSDK_Smaato.xcframework.zip",
            checksum: "f8d541dac118a261407c40bfcb8a81c62f68f3e674a65b45f8a2236068701e09"
            ),
        .binaryTarget(
            name: "SmaatoSDKBanner",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKBanner.xcframework.zip",
            checksum: "cda5d9d3ab7029393f294123af668599c9b23a33eb26667a58dca7fa1ebdb463"
            ),
        .binaryTarget(
            name: "SmaatoSDKCore",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKCore.xcframework.zip",
            checksum: "0b6d10179000d988b77d6f9350d007b2b7a439c25c5d7b6dd22b2b94b6b76a18"
            ),
        .binaryTarget(
            name: "SmaatoSDKInterstitial",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKInterstitial.xcframework.zip",
            checksum: "8df16490c6cd3fffc3f6dba4584a28bd09908deb93e6f292ee45b3e931bb4940"
            ),
        .binaryTarget(
            name: "SmaatoSDKNative",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKNative.xcframework.zip",
            checksum: "11886cf847e2d4e00dfd10597535f1e9dd8e0117f9d5ff9786f3afbb15bfd5bb"
            ),
        .binaryTarget(
            name: "SmaatoSDKOpenMeasurement",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKOpenMeasurement.xcframework.zip",
            checksum: "2982384d1467840db3d8aa31798e276b24f17f2ae7ad3757d9352fef5238623d"
            ),
        .binaryTarget(
            name: "SmaatoSDKRewardedAds",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKRewardedAds.xcframework.zip",
            checksum: "2d92c16e2f1d7649a9f5eb58fb2f63fb8e550c933b03801cac8ccc84ee8c8ec1"
            ),
        .binaryTarget(
            name: "SmaatoSDKRichMedia",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKRichMedia.xcframework.zip",
            checksum: "16125c1305afdc7e5b303f3ce4f2f3863ae14132c39eb5f982b8ea60a79d1d50"
            ),
        .binaryTarget(
            name: "SmaatoSDKVideo",
            url: "https://gitlab.com/freestar/spm-freestarads-smaato/-/raw/22.1.4/SmaatoSDKVideo.xcframework.zip",
            checksum: "c3045d7dda339594bba074cef971a0d1284be97d6c231948d0dda6b28600b1f4"
            ),
    ]
)
